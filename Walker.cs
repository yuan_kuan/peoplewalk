﻿using System;
using System.Collections.Generic;
using UnityEngine;

// The Walker
//
// - Do not stick to a line. they have a rough direction to follow.
// - Not always use the optimal line, especially lot of opposing walkers
// - Usually form lane
// - Some will line up if congested to final destination
// - Mostly do not push
// - Hardly give way to people from behind (cos human do not have back eye.)
// - Will speed up to overtake
// - Will slow down and follow behind
// - Will reduce to normal speed if walk alone, not in rush, not distracted
// - group will walk slower and side by side
// - some get lost
// - some stop abruptly 

namespace PeopleWalk
{

    public struct WalkerProfile
    {
        // Hip size (or breast, whichever larger)
        public float radius;

        // How far ahead to avoid collision
        public float timeHorizon;

        // Fastest speed this walker can walk to the best of her body capability
        public float maxWalkSpeed;

        // The speed she will walk without thinking about walking
        public float normalWalkSpeed;

        // The minimum speed she can tolerate before decide to overtake or start to feel impatient 
        public float followToleranceSpeed;

        // The distance to stranger she prefer to keep while walking alone
        public float frontComfortDistance;
        public float sideComfortDistance;
    }

    public enum WalkerState
    {
        Normal,
        Impatient,
        Overtaking
    }

    public partial class Walker
    {
        public WalkerProfile profile { get; set; }
        public WalkerState state { get; set; }
        public Vector2 targetDirection { get; set; }
        public Vector2 simulatedPosition { get { return m_CurrentPosition; } }

        private Vector2 m_CurrentPosition;
        private Vector2 m_CurrentVelocity;
        private Vector2 m_CurrentAccelaration;

        private List<Walker> m_Neighbours;
        private List<Obstacle> m_Obstacles;

        public bool debugDraw { get; set; }
        public Color debugColor { get; set; }

        public Walker(WalkerProfile profile)
        {
            this.profile = profile;
            this.state = WalkerState.Normal;

            m_Neighbours = new List<Walker>();
            m_Obstacles = new List<Obstacle>();
        }

        public void Simulate(float dt)
        {
            switch(state)
            {
                case WalkerState.Normal:
                    var desiredVelocity = m_CurrentVelocity;

                    // Continue accelaration if not yet reach the normal walking speed
                    if (desiredVelocity.magnitude < profile.normalWalkSpeed)
                        desiredVelocity += m_CurrentAccelaration;


                    desiredVelocity = CalculateRVOVelocity(
                        desiredVelocity, 
                        dt, 
                        profile.normalWalkSpeed, 
                        profile.frontComfortDistance,
                        profile.timeHorizon);
                    
                    // Move
                    m_CurrentVelocity = desiredVelocity;
                    m_CurrentPosition += m_CurrentVelocity * dt;
                    break;
                default:
                    throw new Exception("Unhandled walker state.");
                    break;
            }
        }

 
    #region External Manupulative
    public void SetPosition(Vector2 pos)
        {
            m_CurrentPosition = pos;
        }

        public void SetTargetDirection(Vector2 dir)
        {
            targetDirection = dir;

            if (m_CurrentPosition == null)
                throw new Exception("Set walker's position before setting direciton");

            m_CurrentAccelaration = dir * profile.normalWalkSpeed * 0.1f;
        }
        #endregion External Manupulative
    }
}

