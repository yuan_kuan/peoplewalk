﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PeopleWalk
{
    public class WalkerComp : MonoBehaviour {
    
        public float radius;
        public float normalWalkSpeed;
        public float timeHorizon;
        public float frontComfortDistance;
        public Vector2 targetDirection;

        public bool debug;

        private Walker m_Walker;
        private Transform m_Transform;

	    // Use this for initialization
	    void Start ()
        {
            m_Transform = GetComponent<Transform>();

            var profile = new WalkerProfile();
            profile.radius = radius;
            profile.timeHorizon = timeHorizon;
            profile.frontComfortDistance = frontComfortDistance;
            profile.normalWalkSpeed = normalWalkSpeed;

            m_Walker = new Walker(profile);
            m_Walker.SetPosition(m_Transform.position);
            m_Walker.SetTargetDirection(targetDirection);

            if (debug)
            {
                m_Walker.debugDraw = true;
                m_Walker.debugColor = GetComponent<SpriteRenderer>().color;
            }

            Simulator.instance.AddWalker(m_Walker);
        }
	
	    // Update is called once per frame
	    void Update ()
        {
            m_Transform.position = m_Walker.simulatedPosition;

        }
    }
}
