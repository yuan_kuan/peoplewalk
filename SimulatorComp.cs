﻿using UnityEngine;
using PeopleWalk;

namespace PeopleWalk
{
    public class SimulatorComp : MonoBehaviour
    {
        public int m_TargetFPS;

        private Simulator m_Simulator;

        // Use this for initialization
        void Start()
        {
            m_Simulator = new Simulator(m_TargetFPS);

            var cols = GameObject.FindObjectsOfType<PolygonCollider2D>();
            foreach(var collider in cols)
            {
                var go = collider.gameObject;
                var path = collider.GetPath(0);

                var offset = collider.transform.position;
                for(var i = 0; i < path.Length; ++i)
                {
                    path[i] += (Vector2)offset;
                }
                m_Simulator.AddObstacle(path);
            }
        }

        // Update is called once per frame
        void Update()
        {
            var sim = Simulator.instance;
            sim.Simulate(Time.deltaTime);
        }
    }
}
