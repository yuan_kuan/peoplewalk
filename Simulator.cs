﻿using UnityEngine;
using System.Collections.Generic;

namespace PeopleWalk
{
    public class Obstacle
    {
        public Obstacle next_;
        public Obstacle previous_;
        public Vector2 direction_;
        public Vector2 point_;
        public int id_;
        public bool convex_;
    }

    public class Simulator
    {
        private float m_TargetDeltaTime;
        private float m_AccumulatedTime;

        private List<Walker> m_Walkers;
        private List<Obstacle> m_Obstacle;
        private bool m_WalkerDirty = false;

        public Simulator(int targetFPS)
        {
            m_Walkers = new List<Walker>();
            m_Obstacle = new List<Obstacle>();
            m_TargetDeltaTime = targetFPS / 1000;
            m_AccumulatedTime = 0.0f;

            //if (s_Instance == null)
                s_Instance = this;
        }

        public void Simulate(float dt)
        {
            m_AccumulatedTime += dt;
            if (m_WalkerDirty)
            {
                foreach (var walker in m_Walkers)
                {
                    walker.SetupNeighbours(m_Walkers);
                    walker.SetupObstacle(m_Obstacle);
                }
            }
            if (m_AccumulatedTime >= m_TargetDeltaTime)
            {
                foreach (var walker in m_Walkers)
                    walker.Simulate(m_AccumulatedTime);

                m_AccumulatedTime = 0.0f;
            }
        }

        public void AddWalker(Walker walker)
        {
            m_Walkers.Add(walker);

            m_WalkerDirty = true;
        }

        public int AddObstacle(Vector2[] vertices)
        {
            if (vertices.Length < 2)
            {
                return -1;
            }

            int obstacleNo = m_Obstacle.Count;

            for (int i = 0; i < vertices.Length; ++i)
            {
                Obstacle obstacle = new Obstacle();
                obstacle.point_ = vertices[i];

                if (i != 0)
                {
                    obstacle.previous_ = m_Obstacle[m_Obstacle.Count - 1];
                    obstacle.previous_.next_ = obstacle;
                }

                if (i == vertices.Length - 1)
                {
                    obstacle.next_ = m_Obstacle[obstacleNo];
                    obstacle.next_.previous_ = obstacle;
                }

                obstacle.direction_ = RVOMath.normalize(vertices[(i == vertices.Length - 1 ? 0 : i + 1)] - vertices[i]);

                if (vertices.Length == 2)
                {
                    obstacle.convex_ = true;
                }
                else
                {
                    obstacle.convex_ = (RVOMath.leftOf(vertices[(i == 0 ? vertices.Length - 1 : i - 1)], vertices[i], vertices[(i == vertices.Length - 1 ? 0 : i + 1)]) >= 0.0f);
                }

                obstacle.id_ = m_Obstacle.Count;
                m_Obstacle.Add(obstacle);
            }

            return obstacleNo;
        }

        // Singleton
        static private Simulator s_Instance = null;
        static public Simulator instance
        {
            get
            {
                Debug.Assert(s_Instance != null, "Simulator is not instantiated.");
                return s_Instance;
            }
        }
    }
}
